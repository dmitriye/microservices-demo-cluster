variable "project_id" {
  description = "project id"
}

variable "credentials_file" {
  description = "gce credentials file"
}