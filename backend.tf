terraform {
  backend "gcs" {
    bucket  = "tf-state-gitops"
    prefix  = "terraform/state"
  }
}