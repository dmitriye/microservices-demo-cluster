provider "google" {
  version = "3.5.0"

  project = var.project_id
  region  = "us-central1"
  zone    = "us-central1-c"
}

provider "google-beta" {
  version = "3.5.0"

  project = var.project_id
  region  = "us-central1"
  zone    = "us-central1-c"
}
