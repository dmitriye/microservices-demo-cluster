resource "google_container_cluster" "main" {
    provider    = google-beta
    name        = "my-cluster"
    location    = "us-central1-c"

    remove_default_node_pool = true
    initial_node_count       = 1

    monitoring_service       = "none"
    logging_service          = "none"

    addons_config {
        istio_config {
            disabled = false
        }
        network_policy_config {
            disabled = false
        }
    }

    network_policy {
        enabled = true
        provider = "CALICO"
    }
}

# infra nodes
resource "google_container_node_pool" "infra_nodes" {
    name        = "infra-nodes"
    location    = "us-central1-c"
    cluster     = google_container_cluster.main.name

    node_count  = 2

    node_config {
        machine_type = "n1-standard-2"

        # taint {
        #     key     = "node-role"
        #     value   = "infra"
        #     effect  = "NO_SCHEDULE"
        # }

        metadata = {
            disable-legacy-endpoints = "true"
        }

        oauth_scopes = [
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring",
        ]
    }
}

# worker nodes
resource "google_container_node_pool" "worker_nodes" {
    name        = "worker-nodes"
    location    = "us-central1-c"
    cluster     = google_container_cluster.main.name

    node_count  = 1

    node_config {
        machine_type = "n1-standard-2"

        metadata = {
            disable-legacy-endpoints = "true"
        }

        oauth_scopes = [
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring",
        ]
    }
}